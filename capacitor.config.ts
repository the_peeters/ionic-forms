import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-forms-validation',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
