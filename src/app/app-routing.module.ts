import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/guard/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },

  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule) },

  { path: 'login', loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule) },

  { path: 'profile', loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule),
    canActivate: [AuthGuardService] //Only logged user can enter
  },
  {  path: 'logged-out', loadChildren: () => import('./pages/logout-page/logout-page.module').then( m => m.LogoutPagePageModule)},
  { path: 'signup', loadChildren: () => import('./pages/signup/signup.module').then( m => m.SignupPageModule) },
  { path: 'password-recovery',loadChildren: () => import('./pages/password-recovery/password-recovery.module').then( m => m.PasswordRecoveryPageModule)},
  { path: 'support-form', loadChildren: () => import('./pages/support-form/contact-form.module').then( m => m.ContactFormPageModule) },
  { path: 'about', loadChildren: () => import('./pages/about/about.module').then( m => m.AboutPageModule) },
  { 
    path: 'schedulelist', 
    loadChildren: () => import('./pages/schedulelist/schedulelist.module').then( m => m.SchedulelistPageModule) ,
    canActivate: [AuthGuardService] //Only logged user can enter
  },

  { path: 'about', loadChildren: () => import('./pages/about/about.module').then( m => m.AboutPageModule) },
  //Contact list
  {
    path: 'contactlist',
    loadChildren: () => import('./pages/contactlist/contactlist.module').then( m => m.ContactlistPageModule),
    canActivate: [AuthGuardService] //Only logged user can enter
  },
  {
    path: 'contactlist/add',
    loadChildren: () => import('./pages/contact-add/contact-add.module').then( m => m.ContactAddPageModule),
    canActivate: [AuthGuardService] //Only logged user can enter
  },
  {
    path: 'contactlist/update/:id',
    loadChildren: () => import('./pages/contact-add/contact-add.module').then( m => m.ContactAddPageModule),
    canActivate: [AuthGuardService] //Only logged user can enter
  },
  
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
