import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
// npm install cordova-plugin-splashscreen && npm install @ionic-native/splash-screen
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
// npm install cordova-plugin-statusbar && npm install @ionic-native/status-bar
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


// ionic storage - app component
import { IonicStorageModule } from '@ionic/storage-angular';

// Service to protect views
import { AuthGuardService } from './services/guard/auth-guard.service';
// Service to check authentication
import { AuthenticationService } from './services/auth/authentication.service';
import { ReactiveFormsModule } from '@angular/forms';
// For ngModel
import { FormsModule } from '@angular/forms';  
//for çcontacts filter modal
import { ContactsFilterPageModule } from './pages/contacts-filter/contacts-filter.module';
//Translations
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
//data transfer
import { TransferService } from './services/data/transfer.service';

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    ContactsFilterPageModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    IonicModule.forRoot(), 
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
		IonicStorageModule.forRoot(),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: (createTranslateLoader),
				deps: [HttpClient]
			}
		}),
    ],
  providers: [
    TransferService,
    StatusBar,
    SplashScreen,
    AuthGuardService,
    AuthenticationService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
