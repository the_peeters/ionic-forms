import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';;
import { UsersManagerService } from 'src/app/services/managers/users-manager.service'; 
import { ContactsManagerService } from '../managers/contacts-manager.service';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authState = new BehaviorSubject(false); // use cases beahbiour subject
  currentUser = new BehaviorSubject(null);

  constructor(
    private router: Router,
    public toastController: ToastController,
    public usersManager: UsersManagerService,
  ) { }

  navigatetoLoginPage(){
    this.router.navigate(['login']);
  }

  async login(_user:{}){    
    console.log("Auth service Log In");
    if(await this.usersManager.checkLogin(_user)){
      const response = await this.usersManager.getByMail(_user['email'])
      this.currentUser.next(response);
      this.authState.next(true);
      this.router.navigate(['home']);
    }else{
      this.router.navigate(['home']);
      this.authState.next(false);
    }
  }

  logout(){
    console.log("Auth service Log Out");
    this.currentUser.next(null);
    this.authState.next(false);

    this.router.navigate(['home']);
  }
  
  isAuthenticated(){
    return this.authState.value;
  }
}
