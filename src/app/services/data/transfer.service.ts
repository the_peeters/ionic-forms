import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TransferService {
  data:any;
  constructor() { }
/**
 * to create this service i had to use ionic g service <ServiceName>
 * 
 * Need to put it in the provider 
 * appp.modules.ts
 */


  saveToTransfer(obj:any){
    this.data=obj
  }
  getData(){
    return this.data;
  }
}