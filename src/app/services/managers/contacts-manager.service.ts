import { Injectable, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage/storage.service'; 
import { ContactPerson } from 'src/app/model/ContactModel'
import { Key } from 'selenium-webdriver';
import { AuthenticationService } from '../auth/authentication.service';
@Injectable({
  providedIn: 'root'
})
export class ContactsManagerService{

  //Read -> find in cache 
  //    if cache empty then 
  //        search in storga if is even emty 
  //        return empty

  //write -> cache sync & update to storage async
  //delete -> cache sync 6 delete to storga async


  cache: Array<ContactPerson>;
  constructor(public storageService: StorageService,private authService: AuthenticationService) {

    this.authService.currentUser.subscribe(userInfo=>{
      if(userInfo==null){
        this.cache = new Array<ContactPerson>();
      }
    });
  }

  async getContacts(id:string=""){
    console.log("ContactsManagerService id->",id);
    
    if (this.cache!=undefined && this.cache.length!=0){
      console.log("Found contacts for "+id+" in cache");
      return this.cache;
    }else{
      this.cache = await this.storageService.usersContactsTable.get(id);
      if (this.cache) {
        console.log("Found in local storage for "+id);
        console.log(this.cache);
        return this.cache;
      } else {
        console.log("Not found contacts in Local storage for "+id);
        return new Array();
      }
    }
  }
  addContact(userId: string, contact: ContactPerson) {
    //Add to cache to rapid update then save into storage
    console.log("Added");
    if (this.cache ==null || this.cache==undefined){
      this.cache = new Array();
    }
    this.cache.push(contact);
    
    console.log("Updating contacts into UserContactsTable for "+userId);
    this.storageService.usersContactsTable.set(userId,this.cache);
 
  }

}
