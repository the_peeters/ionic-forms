import { Injectable } from '@angular/core';
import { StorageService } from 'src/app/services/storage/storage.service'; 
@Injectable({
  providedIn: 'root'
})
export class UsersManagerService {
  usersDB: any;
  constructor(private storage2: StorageService  ) {
    this.usersDB = storage2;

  }

  setUser(_user: {}) {
    if(_user){
      this.usersDB.usersTable.set(_user['email'],_user)
    }
  }
  async checkLogin(_user: {}) {
   
    const objusr = await   this.usersDB.usersTable.get(_user['email']); 
    if(objusr!=null){
      return (objusr['passwd']==_user['passwd']) ? true : false;
    }else{
      return false;
    }
  }
  async getByMail(key: string) {
    const usr =  await this.usersDB.usersTable.get(key);
    return usr;
  }
  async updateUser(u_user:{}) {
    if(u_user){
      console.log("Update finish");
      await this.usersDB.usersTable.set(u_user['email'],u_user);


    }

  }
}
