import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public authenticator: AuthenticationService) { }
  canActivate():any{
    return this.authenticator.isAuthenticated();
  }
}
