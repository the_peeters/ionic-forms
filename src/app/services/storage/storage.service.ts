import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  usersContactsTable:any;
  usersScheduleTable: any;
  usersTable:any;

  constructor() {
    this.usersContactsTable =new Storage(
      {
        name: '__ionicApp_db',
        storeName: '_contacts',
        dbKey: '_ionickey', 
        driverOrder:  [ "ionicSecureStorage", "asyncStorage", "localStorageWrapper" ]
      }
    )
    this.usersScheduleTable =new Storage(
      {
        name: '__ionicApp_db',
        storeName: '_schedules',
        dbKey: '_ionickey', 
        driverOrder:  [ "ionicSecureStorage", "asyncStorage", "localStorageWrapper" ]
      }
    )
    this.usersTable =new Storage(
      {
        name: '__ionicApp_db',
        storeName: '_users',
        dbKey: '_ionickey', 
        driverOrder:  [ "ionicSecureStorage", "asyncStorage", "localStorageWrapper" ]
      }
    )
    

    
  }
  create(){
    this.usersContactsTable.create();
    this.usersScheduleTable.create();
    this.usersTable.create();
    return this;

  }
}
