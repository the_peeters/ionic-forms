export class UserPerson {
    fname: string;
    lname: string;
    email:string;
    passwd: string;
    dob: string="";
    phone:string="";
    id:string;
    constructor(
        fname: string,lname: string,email: string,
        passwd: string,dob: string="",phone: string="") {
        this.fname =fname;
        this.lname=lname;
        this.email=email;
        this.passwd=passwd;
        this.dob=dob;
        this.phone=phone;
        this.id=this.generateId(email);
    }
    generateId(email: string): string {
        return email;
    }
    
    public toString(): string {
        return ( 
            "------ User registration ------\n"+
            "fname: "+this.fname+"\n"+
            "lname: "+this.lname+"\n"+
            "email: "+this.email+"\n"+
            "passwd: "+this.passwd+"\n"+
            "dob: "+this.dob+"\n"+
            "phone: "+this.phone+"\n"+
            "id: "+this.id+"\n"+
            "---------------------"
        );
    }
}