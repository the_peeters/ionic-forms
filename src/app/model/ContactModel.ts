export class ContactPerson {
    fname: string ="";
    lname: string ="";
    phone:string="";
    email:string;
    avatar: string="";
    type: string="";
    comment:string="";
    constructor(fname: string,lname: string="",phone: string="",email:string="",avatar: string ="",type: string="",comment: string="") {
        this.fname =fname;
        this.lname=lname;
        this.phone=phone;
        this.email=email;
        this.avatar=avatar;
        this.type=type;
        this.comment=comment;
    }
    
    public toString(): string {
        return ( 
            "------ user contact add ------\n"+
            "fname: "+this.fname+"\n"+
            "lname: "+this.lname+"\n"+
            "phone: "+this.phone+"\n"+
            "avatar: "+this.avatar+"\n"+
            "type: "+this.type+"\n"+
            "comment: "+this.comment+"\n"+
            "---------------------"
        );
    }
}