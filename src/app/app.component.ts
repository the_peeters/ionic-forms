import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

import { AuthenticationService } from './services/auth/authentication.service';
import { TranslateService } from '@ngx-translate/core';

import { StorageService } from 'src/app/services/storage/storage.service'; 
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  darkMode=false;
  loggedIn=false;

  storage_service:any;
  constructor(
    
    private storage2:StorageService,
    private authService: AuthenticationService,
    private translate: TranslateService
  ){
      this.storage_service = storage2.create();
      const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
      this.darkMode = prefersDark.matches;
      this.translate.setDefaultLang('ca'); 
  }

  ngOnInit() {
    this.authService.authState.subscribe(data =>{
      this.loggedIn = data;
    });
   
  }
  openTutorial(){
    console.log("Open tutorial")
  }
  logout(){
    console.log("App Log Out");
    this.authService.logout();
    
  }
  toggleDarkMode() {
    if(!this.darkMode){
      console.log("Dark mode enabled");
    }else{
      console.log("Dark mode disabled");
    }
    this.darkMode = !this.darkMode;
    document.body.classList.toggle( 'dark' );
    
  }
 
}
