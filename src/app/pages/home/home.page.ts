import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/auth/authentication.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  loggedIn:Boolean;
  user:string;
  params = {name:"BaTmAn"}
  constructor( 
    private authService: AuthenticationService,
    public storage: Storage,
  ) { 

  }
  ngOnInit() {

    this.authService.authState.subscribe(data =>{
      this.loggedIn = data;
    });

    this.authService.currentUser.subscribe(user_info=>{
      if(user_info!=null){
        this.user=user_info['fname'];
      }
    });
  }

  logoutUser(){
    this.authService.logout();
  }
  
}
