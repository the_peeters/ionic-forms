import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/auth/authentication.service';
import { UsersManagerService } from 'src/app/services/managers/users-manager.service'; 
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserPerson } from 'src/app/model/UserModel';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  isSubmitted = false;

  get fname() { return this.loginForm.get('fname'); }

  error_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email address.' }
    ],

    'password': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password must have minium 8 length.'},
    ],
  }

  constructor(
    private authService: AuthenticationService,
    public formBuilder: FormBuilder,
    public usersManager: UsersManagerService,
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['',[Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{1,3}$')]],
      password: ['', [Validators.required,Validators.minLength(3)]],
    });
  }

  ngOnInit() {
    let default_user = new UserPerson("Test","Test surname","test@test","123456");
    this.usersManager.setUser(default_user);
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.loginForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      let formResult=this.loginForm.value;
      let login_user = {
        'email':formResult['email'],
        'passwd':formResult['password']
      };
   
      this.authService.login(login_user);
    }
  }
}
