import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SchedulelistPage } from './schedulelist.page';

const routes: Routes = [
  {
    path: '',
    component: SchedulelistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SchedulelistPageRoutingModule {}
