import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SchedulelistPageRoutingModule } from './schedulelist-routing.module';

import { SchedulelistPage } from './schedulelist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SchedulelistPageRoutingModule
  ],
  declarations: [SchedulelistPage]
})
export class SchedulelistPageModule {}
