import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/auth/authentication.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Storage } from '@ionic/storage';
import { UsersManagerService } from 'src/app/services/managers/users-manager.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  userInfoForm: FormGroup;
  defaultDate: string;
  providedMail: string;
  isSubmitted = false;
  UserDetails: any;

  error_messages = {
    'fname': [
      { type: 'required', message: 'First Name is required.' },
      { type: 'maxlength', message: 'First Name too large' },
    ],
    'lname': [
      { type: 'required', message: ' Last Name is required.' },
      { type: 'maxlength', message: 'Last Name too large' },
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'please enter a valid email address.' }
    ],

    'mobile': [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Only numerical values allowed' },
    ],
  }
  constructor(
    public formBuilder: FormBuilder,
    private authService: AuthenticationService,
    public usersManager: UsersManagerService,
  ) {

    this.authService.currentUser.subscribe(user_info => {
      if (user_info != null) {
        this.UserDetails = user_info;
      }
    });

  }

  logoutUser() {
    this.authService.logout();
  }


  getDefaultDate(): string {
    let date = new Date().toLocaleString().substring(0, 10);
    const dateStr = date.split("/").reverse().join("-");
    console.log(dateStr);
    return dateStr;
  }

  ngOnInit() {

    this.userInfoForm = this.formBuilder.group({
      fname: [this.UserDetails['fname'], [Validators.required, Validators.maxLength(255)]],
      lname: [this.UserDetails['lname'], [Validators.required, Validators.maxLength(255)]],
      dob: [this.UserDetails['dob'], Validators.required],
      phone: [this.UserDetails['phone'], [Validators.required, Validators.pattern('^[0-9]+$')]]
    })
  }


  get errorControl() {
    return this.userInfoForm.controls;
  }

  submitForm(): boolean {
    this.isSubmitted = true;
    if (!this.userInfoForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      let formResult = this.userInfoForm.value;
      if (this.checkChanges(formResult)) {
        this.usersManager.updateUser(this.UserDetails);
      }
      return true;
    }
  }

  checkChanges(formResult: any): boolean {
    if (
      this.UserDetails['dob'] == formResult['dob'] &&
      this.UserDetails['fname'] == formResult['fname'] &&
      this.UserDetails['lname'] == formResult['lname'] &&
      this.UserDetails['phone'] == formResult['phone']
    ) {
      console.log("No changes were made, no update pending");
      return false;
    }
    console.log("Some changes were made,updating new changes");

    console.log("Old:", this.UserDetails['dob'], "new:", formResult['dob']);
    console.log("Old:", this.UserDetails['fname'], "new:", formResult['fname']);
    console.log("Old:", this.UserDetails['lname'], "new:", formResult['lname']);
    console.log("Old:", this.UserDetails['phone'], "new:", formResult['phone']);

    this.UserDetails['dob'] = formResult['dob'];
    this.UserDetails['fname'] = formResult['fname'];
    this.UserDetails['lname'] = formResult['lname'];
    this.UserDetails['phone'] = formResult['phone'];


    return true;


  }

  getDate(e) {
    let date = new Date(e.target.value).toISOString().substring(0, 10);
    this.userInfoForm.get('dob').setValue(date, { onlyself: true })
  }


}
