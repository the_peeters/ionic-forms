import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilePageRoutingModule } from './profile-routing.module';

import { ProfilePage } from './profile.page';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
//Added Trnslations
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilePageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    RouterModule.forChild([
			{
				path: "",
				component: ProfilePage,
			},
		]),
  ],
  declarations: [ProfilePage]
})
export class ProfilePageModule {}
