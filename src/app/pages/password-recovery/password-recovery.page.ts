import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { FormControl, ValidationErrors, ValidatorFn } from '@angular/forms'; // add  ReactiveFormsModule to register-page.modules.ts
import { AuthenticationService } from '../../services/auth/authentication.service'; 

import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.page.html',
  styleUrls: ['./password-recovery.page.scss'],
})
export class PasswordRecoveryPage implements OnInit {
  loginForm: FormGroup;
  isSubmitted = false;

  get fname() { return this.loginForm.get('fname'); }

  error_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'please enter a valid email address.' }
    ],
  }

  constructor(
    public formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['',[Validators.required,Validators.email]],
    });

  }

  ngOnInit() {
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.loginForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      
      this.send_information();

    }
  }
  send_information() {
    console.log(this.loginForm.value)
    console.log("Sending link")

    
  }

}
