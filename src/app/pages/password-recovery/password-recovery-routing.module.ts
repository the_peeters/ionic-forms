import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswordRecoveryPage } from './password-recovery.page';

const routes: Routes = [
  {path: '', component: PasswordRecoveryPage},
  {path: 'done',loadChildren: () => import('./../password-recovery-done/password-recovery-done.module').then( m => m.PasswordRecoveryDonePageModule)}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswordRecoveryPageRoutingModule {}
