import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasswordRecoveryPageRoutingModule } from './password-recovery-routing.module';

import { PasswordRecoveryPage } from './password-recovery.page';
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
//Added Trnslations
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswordRecoveryPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    RouterModule.forChild([
			{
				path: "",
				component: PasswordRecoveryPage,
			},
		]),
  ],
  declarations: [PasswordRecoveryPage]
})
export class PasswordRecoveryPageModule {} 
