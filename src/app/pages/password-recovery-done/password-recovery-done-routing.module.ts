import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswordRecoveryDonePage } from './password-recovery-done.page';

const routes: Routes = [
  {
    path: '',
    component: PasswordRecoveryDonePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswordRecoveryDonePageRoutingModule {}
