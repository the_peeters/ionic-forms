import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-password-recovery-done',
  templateUrl: './password-recovery-done.page.html',
  styleUrls: ['./password-recovery-done.page.scss'],
})
export class PasswordRecoveryDonePage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
