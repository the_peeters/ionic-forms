import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasswordRecoveryDonePageRoutingModule } from './password-recovery-done-routing.module';

import { PasswordRecoveryDonePage } from './password-recovery-done.page';
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
//Added Trnslations
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswordRecoveryDonePageRoutingModule,
    TranslateModule,
    RouterModule.forChild([
			{
				path: "",
				component: PasswordRecoveryDonePage,
			},
		]),
  ],
  declarations: [PasswordRecoveryDonePage]
})
export class PasswordRecoveryDonePageModule {}
