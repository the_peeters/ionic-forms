import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage/storage.service';
import { ContactPerson } from 'src/app/model/ContactModel'
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactsManagerService } from 'src/app/services/managers/contacts-manager.service';
import { ActivatedRoute } from '@angular/router';
import { TransferService } from 'src/app/services/data/transfer.service';
@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.page.html',
  styleUrls: ['./contact-add.page.scss'],
})
export class ContactAddPage implements OnInit {
  newContactForm: FormGroup;
  currentUserId:string;
  isSubmitted:boolean;

  willUpdate:boolean=false;
  userToUpdate:ContactPerson;
  error_messages = {
    'fname': [
      { type: 'required', message: 'First Name is required.' },
      { type: 'maxlength', message: 'First Name too large' },
    ],
    'lname': [
      { type: 'required', message: ' Last Name is required.' },
      { type: 'maxlength', message: 'Last Name too large' },
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'please enter a valid email address.' }
    ],

    'mobile': [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Only numerical values allowed' },
    ],
  }

  constructor(
    private authService: AuthenticationService,
    public formBuilder: FormBuilder, 
    public contactsManager: ContactsManagerService,
    private route: ActivatedRoute,
    public dataTransfer: TransferService 
    ) {
    this.route.params.subscribe( (params: any) => {
      console.log("parameters =>",params);
      //data transfer
      console.log("data =>",this.dataTransfer.getData());
      this.userToUpdate = this.dataTransfer.getData();
      
    });

    this.authService.currentUser.subscribe(userInfo=>{
      if(userInfo!=null){        
        this.currentUserId=userInfo['id'];
      }else{
        this.currentUserId="";
      }
    });
   }
   

  ngOnInit() {
    console.log((this.userToUpdate) ? "Update mode": "add mode");
    
    let fnameVal=(this.userToUpdate) ? this.userToUpdate['fname']: '';
    let lnameVal=(this.userToUpdate) ? this.userToUpdate['lname']: '';
    let emailVal=(this.userToUpdate) ? this.userToUpdate['email']: '';
    let phoneVal=(this.userToUpdate) ? this.userToUpdate['phone']: '';
    let typeVal=(this.userToUpdate) ? this.userToUpdate['type']: '';
    let commentVal=(this.userToUpdate) ? this.userToUpdate['comment']: '';

    this.newContactForm = this.formBuilder.group({
      fname: [fnameVal, [Validators.required, Validators.maxLength(255)]],
      lname: [lnameVal, [Validators.required, Validators.maxLength(255)]],
      email: [emailVal, [Validators.required]],
      phone: [phoneVal, [Validators.required]],
      type: [typeVal, []],
      comment: [commentVal, []]
      
    })
  }
  ionViewWillEnter(){
    
  }
  submitForm(){
    this.isSubmitted = true;
    if (!this.newContactForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      let form=this.newContactForm.value;
      let new_contact= new ContactPerson(form["fname"],form["lname"],form["phone"],form["email"],form["avatar"],form["type"],"comment");
      console.log(form);
      this.contactsManager.addContact(this.currentUserId,new_contact)
    }
  }
 

}