import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactAddPageRoutingModule } from './contact-add-routing.module';

import { ContactAddPage } from './contact-add.page';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
//Added trnslation
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactAddPageRoutingModule,
    ReactiveFormsModule, //checking forms
    TranslateModule,
    RouterModule.forChild([
			{
				path: "",
				component: ContactAddPage,
			},
		]),
  ],
  declarations: [ContactAddPage]
})
export class ContactAddPageModule {}
