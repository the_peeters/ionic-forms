import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactFormPageRoutingModule } from './contact-form-routing.module';

import { ContactFormPage } from './contact-form.page';

import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
//Added Trnslations
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactFormPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    RouterModule.forChild([
			{
				path: "",
				component: ContactFormPage,
			},
		]),
  ],
  declarations: [ContactFormPage]
})
export class ContactFormPageModule {}
