import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/auth/authentication.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.page.html',
  styleUrls: ['./contact-form.page.scss'],
})
export class ContactFormPage implements OnInit {
  contactForm: FormGroup;
  isSubmitted = false;

  get fname() { return this.contactForm.get('fname'); }

  error_messages = {
    'fname': [
      { type: 'required', message: 'Name is required' },
      { type: 'maxlength', message: 'Too large' },
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'please enter a valid email address.' }
    ],

    'textbox': [
      { type: 'required', message: 'Write something' },
    ],
  }

  constructor(
    private authService: AuthenticationService,
    public formBuilder: FormBuilder
  ) {
    this.contactForm = this.formBuilder.group({
      fname: ['', [Validators.required,Validators.maxLength(255)]],
      email: ['',[Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{1,3}$')]],
      textbox: ['',[Validators.required]]
    });
  }

  ngOnInit() {
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.contactForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.contactForm.value)

      let element = document.getElementById("form-contact");
      let newElement = document.createElement("div");
      newElement.innerHTML = "<div class='content-container'><h1>Message Sent</h1></div>";
      element.parentNode.replaceChild(newElement, element);
    }
  }
}
