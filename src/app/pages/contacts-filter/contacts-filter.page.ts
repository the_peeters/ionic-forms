import { Component } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ContactPerson } from 'src/app/model/ContactModel';
import { ContactsManagerService } from 'src/app/services/managers/contacts-manager.service';

@Component({
  selector: 'app-contacts-filter',
  templateUrl: './contacts-filter.page.html',
  styleUrls: ['./contacts-filter.page.scss'],
})
export class ContactsFilterPage {
  tracks: { name: string, icon: string, isChecked: boolean }[] = [
    { "name": "A", icon: "string", isChecked: true },
    { "name": "B", icon: "string", isChecked: true },
    { "name": "C", icon: "string", isChecked: true }
  ];

  tmpContacts: Array<ContactPerson> = []
  userId: string = "";
  constructor(
    public modalCtrl: ModalController,
    public navParams: NavParams,
    private contactsManager: ContactsManagerService

  ) {
    this.userId = this.navParams.get("currentId")
  }



  async ionViewWillEnter() {
    console.log("Modal user =>",this.userId);
    
    this.tmpContacts = await this.contactsManager.getContacts(this.userId);
  }


  selectAll(option: boolean): void {
    this.tracks.forEach(item => {
      item.isChecked = option;
    });
  }

  checkCategory(array: string[], length: number, result: boolean, category: string): boolean {
    if (length <= 0) {
      return result;
    }
    return (
      this.checkCategory(array, length - 1, result || (array[length - 1] == category), category)
    );

  }



  selectByCategory = (selected: string[], category: string) => {
    return (
      this.checkCategory(selected, selected.length, false, category)
    );
  }


  applyFilters(): void {
    let selected = this.tracks.filter(c => c.isChecked).map(c => c.name);
    console.log("selected =>", selected)

    let result = this.tmpContacts.filter(person => {
      return this.selectByCategory(selected, person.type);
    });
    
    this.dismiss(result);
  }

  dismiss(data?: any) {
    this.modalCtrl.dismiss(data);
  }

}