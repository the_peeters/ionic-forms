import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactlistPageRoutingModule } from './contactlist-routing.module';

import { ContactlistPage } from './contactlist.page';
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
//Added Trnslations
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactlistPageRoutingModule,
    TranslateModule,
    RouterModule.forChild([
			{
				path: "",
				component: ContactlistPage,
			},
		]),
  ],
  declarations: [ContactlistPage]
})
export class ContactlistPageModule {}
