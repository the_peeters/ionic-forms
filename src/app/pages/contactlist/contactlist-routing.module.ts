import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactlistPage } from './contactlist.page';

const routes: Routes = [
  {
    path: '',
    component: ContactlistPage
  },
  //{path: 'add',loadChildren: () => import('./../contact-add/contact-add.module').then( m => m.ContactAddPageModule)}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContactlistPageRoutingModule {}
