import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController} from '@ionic/angular';
import { ContactPerson } from 'src/app/model/ContactModel';
import { TransferService } from 'src/app/services/data/transfer.service';
import { ContactsManagerService } from 'src/app/services/managers/contacts-manager.service';
import { AuthenticationService } from '../../services/auth/authentication.service';
import { ContactsFilterPage } from '../contacts-filter/contacts-filter.page';

@Component({
  selector: 'app-contactlist',
  templateUrl: './contactlist.page.html',
  styleUrls: ['./contactlist.page.scss'],
})
export class ContactlistPage implements OnInit {
  showSearchbar:boolean;
  queryText = '';
  contactsUserArray=[];
  currentUserId:any;


  constructor(
    private contactsManager: ContactsManagerService,
    private authService: AuthenticationService,
    public modalCtrl: ModalController,
    private router: Router,
    public dataTransfer: TransferService  
    ) {
    this.authService.currentUser.subscribe(userInfo=>{
      if(userInfo!=null){
        this.currentUserId=userInfo['id'];
      }
    });
  }

  ngOnInit() {
    console.log("On init ContactlistPage");
  }

  async ionViewWillEnter(){
    console.log("ionViewWillEnter" + this.queryText);
    console.log("getting contacts for "+this.currentUserId);
   
    
    this.contactsUserArray = await this.contactsManager.getContacts(this.currentUserId);
  }
  ionViewDidEnter(){
    console.log("ionViewDidEnter");
  }
  ionViewWillLeave(){
    console.log("ionViewWillLeave");
  }
  ionViewDidLeave(){
    console.log("ionViewDidLeave");
  }
  async updateContactList(){
    if (this.queryText != ""){      
      console.log("Query =>",this.queryText.toLowerCase(),"<="); 
      this.contactsUserArray = this.contactsUserArray.filter(person => {
          return this.searchByTerm(person,this.queryText.toLowerCase());
      }); 
    }else{
      console.log("Querry =>","<empty string>","<=");
      this.contactsUserArray = await this.contactsManager.getContacts(this.currentUserId);
    }
    console.log(this.contactsUserArray);
  }

  searchByTerm = (person, searchTerm) => {
    return (
      (person.fname.toLowerCase()).includes(searchTerm) ||
      (person.lname.toLowerCase()).includes(searchTerm) ||
      (person.phone.toLowerCase()).includes(searchTerm) ||
      (person.email.toLowerCase()).includes(searchTerm)
    );
  }
  
  async presentFilter() {
    console.log("current user will enter into Modal =>",this.currentUserId);
    const modal = await this.modalCtrl.create({
      component: ContactsFilterPage,
      swipeToClose: true,
      componentProps: { currentId: this.currentUserId}
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.contactsUserArray = data;
    }
  }
  navigateToDetails(index:any){
    //contactlist/:op/:id
    this.dataTransfer.saveToTransfer(this.contactsUserArray[index])
    this.router.navigate(["/contactlist/update",index]);
    
  }
}
