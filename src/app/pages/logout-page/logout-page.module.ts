import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LogoutPagePageRoutingModule } from './logout-page-routing.module';

import { LogoutPagePage } from './logout-page.page';
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
//Added Trnslations
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogoutPagePageRoutingModule,
    TranslateModule,
    RouterModule.forChild([
			{
				path: "",
				component: LogoutPagePage,
			},
		]),
  ],
  declarations: [LogoutPagePage]
})
export class LogoutPagePageModule {}
 