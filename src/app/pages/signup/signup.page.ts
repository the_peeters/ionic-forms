import { Component, OnInit } from '@angular/core';

import { ToastController } from '@ionic/angular';
import { ValidationErrors, ValidatorFn } from '@angular/forms'; // add  ReactiveFormsModule to register-page.modules.ts

import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UsersManagerService } from 'src/app/services/managers/users-manager.service'; 
import { UserPerson } from 'src/app/model/UserModel';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})


export class SignupPage implements OnInit {
  loginForm: FormGroup;
  isSubmitted = false;

  error_messages = {
    'fname': [
      { type: 'required', message: 'First Name is required.' },
    ],

    'lname': [
      { type: 'required', message: 'Last Name is required.' }
    ],

    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'please enter a valid email address.' }
    ],

    'password1': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password must have minium 6 length.'},
    ],
    'password2': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password must have minium 6 length.' },
    ],
  }

  constructor(
    public formBuilder: FormBuilder,
    public usersManager: UsersManagerService,
  ) {
    this.loginForm = this.formBuilder.group({
      fname: ['', [Validators.required,Validators.minLength(3)]],
      lname: ['', [Validators.required]],
      email: ['',[Validators.required,Validators.email]],
      password1: ['', [Validators.required,Validators.minLength(6)]],
      password2: ['', [Validators.required,Validators.minLength(6)]],
      
    });
    this.loginForm.setValidators(this.password_checker());

  }

  ngOnInit() {
  }

  password_checker(): ValidatorFn {
    return (formGroup: FormGroup): ValidationErrors => {
      const { value: password } = formGroup.get('password1');
      const { value: confirmPassword } = formGroup.get('password2');
      if(password === confirmPassword) return null;
      else return { passwordNotMatch: true };
    };
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.loginForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      let formresult=this.loginForm.value;

      let new_user = new UserPerson(formresult['fname'],formresult['lname'],formresult['email'],formresult['password1']);
      console.log(new_user);
      this.usersManager.setUser(new_user);
    }
  }
}
